package com.demo.demo01.config;

import com.demo.demo01.dao.UserDao;
import com.demo.demo01.pojo.User;
import com.demo.demo01.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


//拦截器在进行执行时bean还没初始化，所以自动注入不管用，会报空指针
public class Interceptor implements HandlerInterceptor {

    private UserService userService;

    public Interceptor(UserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {


        String authorization = request.getHeader("Authorization");

        if (authorization==null) {
            System.out.println("uuid为空，拦截");
            return false;
        }
        else {
            User getuser = userService.getuser(authorization);
            if(getuser==null){
                System.out.println("该用户不存在，拦截");
                return false;
            }
            else {
                return true;
            }
        }

//        return true;

    }
}
