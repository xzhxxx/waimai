package com.demo.demo01.pojo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Address {
    private String uuid;
    private String contractperson;
    private String contractphone;
    private String address;
    private String detailaddress;


}
