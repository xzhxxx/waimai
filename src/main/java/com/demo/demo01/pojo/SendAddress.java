package com.demo.demo01.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SendAddress {
    private String address;
    private String sendtowho;
    private String phone;
    private String totalprice;
}
