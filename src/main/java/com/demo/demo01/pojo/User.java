package com.demo.demo01.pojo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private String uuid;
    private  String username;
    private String password;
    private  String phone;

    private String nicheng;
    private String iconpath;

}
