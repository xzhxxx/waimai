package com.demo.demo01.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetail {
    private String orderid;
    private String shopid;
    private String address;
    private String sendtowho;
    private String phone;
    private String totalprice;
    private String createtime;
    private String shopname;
    private String shoplogopath;
    private String state;
    private List<DetailProduct> detailProducts;
}
