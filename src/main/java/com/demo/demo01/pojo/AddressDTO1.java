package com.demo.demo01.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddressDTO1 {
    private String addressid;
    private String contractperson;
    private String contractphone;
    private String address;
    private String detailaddress;
}
