package com.demo.demo01.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDTO1 {

    private String itemid;
    private String itemprice;
    private String currentnum;
    private String itempic;
    private String itemname;
}
