package com.demo.demo01.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product {

    private String thingid;
    private String thingname;
    private String thingdesc;
    private String thingprice;
    private String thingpic;
}
