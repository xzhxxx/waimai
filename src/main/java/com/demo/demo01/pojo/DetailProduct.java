package com.demo.demo01.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DetailProduct {
    private String productid;
    private String productname;
    private String productprice;
    private String productcount;
    private String productpic;
}
