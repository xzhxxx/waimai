package com.demo.demo01.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Order {
    private String shopname;
    private String state;
    private String shoplogopath;
    private String totalprice;
    private String totalcount;
    private List<ProductDTO2> productDTO2;
}
