package com.demo.demo01.service;

import com.demo.demo01.pojo.Product;
import com.demo.demo01.pojo.Shop;

import java.util.List;

public interface ShopService {
    List<Shop> getShops();

    List<Product> getShopDetail(String shopuuid);

    String shopHeadPic(String shopid);

}
