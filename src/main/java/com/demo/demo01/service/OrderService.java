package com.demo.demo01.service;

import com.demo.demo01.pojo.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderService {
    int createOrder(String orderid,String submiterid,String shopid,String totalcount,SendAddress sendAddress,List<ProductDTO1> productDTO1);

    List<Orders> getorders(String token);

    OrderDetail getcurrentOrder(String token,String orderid);

    //修改订单状态(支付完成)
    int UpdateOrder_payed(String orderid);


    //查找当前用户的已下单订单
    List<Orders> getorders_finished(String token,String state);

    List<Orders> getorders_xiadan_send(String token,String state,String state2);


    //收货操作
    int ihaveget(String token,String orderid);

}
