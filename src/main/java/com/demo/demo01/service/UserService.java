package com.demo.demo01.service;

import com.demo.demo01.pojo.Address;
import com.demo.demo01.pojo.AddressDTO1;
import com.demo.demo01.pojo.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserService {
    int Reguser(com.demo.demo01.pojo.User user);
    User login(String username);
    int modifyHead(String uuid, String iconpath);
    int modifynicheng(String uuid, String newnicheng);

    int  addAddress(Address address);
    List<AddressDTO1> Getaddresses(String token);
    int modifyAddress(String uuid,AddressDTO1 dto1);
    int deleteAddress(String uuid,String addressid);
    //查找有没有这个人
    User getuser(String uuid);


}
