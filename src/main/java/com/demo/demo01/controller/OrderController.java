package com.demo.demo01.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.demo.demo01.pojo.*;
import com.demo.demo01.service.OrderService;
import com.demo.demo01.utils.Dictionary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
public class OrderController {

    @Autowired
    OrderService orderService;

    /**
     * 提交订单
     * @param products
     * @param token
     * @param sendAddress
     * @param shopid
     * @return
     */
    @PostMapping("/order/submit")
    public String submitOrder(@RequestParam("products") String products, @RequestHeader("Authorization")String token, @RequestParam("addressInfo") String sendAddress,@RequestParam("shopid")String shopid,@RequestParam("totalcount")String totalcount){
        JSONArray jsonArray = JSON.parseArray(products);
        List<ProductDTO1> list=new ArrayList<>();

        for (int i = 0; i <jsonArray.size() ; i++) {
            list.add(JSON.toJavaObject((JSON) jsonArray.get(i),ProductDTO1.class) );
        }

        System.out.println(list.toString());

        JSONObject jsonObject = (JSONObject) JSON.parse(sendAddress);
        SendAddress sendAddress1 = new SendAddress();
        sendAddress1.setAddress( jsonObject.getString("address"));
        sendAddress1.setSendtowho( jsonObject.getString("sendtowho"));
        sendAddress1.setPhone( jsonObject.getString("phone"));
        sendAddress1.setTotalprice( jsonObject.getString("totalprice"));

        System.out.println(sendAddress1.toString());
//        System.out.println(jsonArray);
//        System.out.println(token);

        String ORDERID= String.valueOf(UUID.randomUUID());

        int result = orderService.createOrder(ORDERID,token,shopid,totalcount,sendAddress1,list);
        if(result>0){
            return ORDERID;
        }

        else {
            return "fail";
        }


    }


    /**
     * 获取全部订单
     * @param token
     * @return
     */
    @GetMapping("/order/getallorder")
    public String getallOrder(@RequestHeader("Authorization")String token){

            List<Orders> getorders = orderService.getorders(token);
            System.out.println(getorders);
            for (Orders o:getorders
            ) {
                String shoplogopath = o.getOrders().get(0).getShoplogopath();
                String substring = shoplogopath.substring(27);
                String newlogopath=Dictionary.ip+substring;
                o.getOrders().get(0).setShoplogopath(newlogopath);
            }
            System.out.println(getorders);
            Object o = JSON.toJSON(getorders);
            String orders = JSON.toJSONString(o);
            System.out.println(orders);

            return orders;

    }


    /**
     * 获取当前订单
     * @param token
     * @param orderid
     * @return
     */
    @GetMapping("/order/currentorder")
    public String currentorderdetail(@RequestHeader("Authorization")String token,String orderid){
        OrderDetail orderDetail = orderService.getcurrentOrder(token, orderid);

        String shoplogopath = orderDetail.getShoplogopath();
        String substring = shoplogopath.substring(27);
        String logopath=Dictionary.ip+substring;
        orderDetail.setShoplogopath(logopath);

        System.out.println(JSON.toJSONString(orderDetail));
        return JSON.toJSONString(orderDetail);
    }


    /**
     *支付订单
     */

    @PostMapping("/order/pay")
    public String modifyOrderState(@RequestHeader("Authorization")String token,String orderid,String flag){
        if ("pay".equals(flag)){//支付完成，修改订单状态为已下单
            int i = orderService.UpdateOrder_payed(orderid);
            if (i>0){
                System.out.println("订单状态已变更为：已下单");
                return "success";
            }
            else {
                return "fail";
            }
        }

        else {
            return "待制作";
        }

    }


    /**
     * 查找已付钱的订单(已下单/已送出)
     * @param token
     * @return
     */
    @GetMapping("/order/orders_finished")
    public String getFinishedorders(@RequestHeader("Authorization")String token){
        List<Orders> orders = orderService.getorders_xiadan_send(token,"已下单","已送出");
        System.out.println(orders);
        for (Orders o:orders
        ) {
            String shoplogopath = o.getOrders().get(0).getShoplogopath();
            String substring = shoplogopath.substring(27);
            String newlogopath=Dictionary.ip+substring;
            o.getOrders().get(0).setShoplogopath(newlogopath);
        }
        System.out.println(orders);
        Object o = JSON.toJSON(orders);
        String orders2 = JSON.toJSONString(o);
        System.out.println(orders2);

        return orders2;

    }


    /**
     * 查找还没付钱的订单
     * @param token
     * @return
     */
    @GetMapping("/order/orders_havenotpay")
    public String gethavenotpayorders(@RequestHeader("Authorization")String token){
        List<Orders> orders = orderService.getorders_finished(token,"待支付");
        System.out.println(orders);
        for (Orders o:orders
        ) {
            String shoplogopath = o.getOrders().get(0).getShoplogopath();
            String substring = shoplogopath.substring(27);
            String newlogopath=Dictionary.ip+substring;
            o.getOrders().get(0).setShoplogopath(newlogopath);
        }
        System.out.println(orders);
        Object o = JSON.toJSON(orders);
        String orders2 = JSON.toJSONString(o);
        System.out.println(orders2);

        return orders2;

    }


    /**
     * 我要收货的操作
     * @param token
     * @param orderid
     * @return
     */
    @PostMapping("/order/haveget")
    public String haveget(@RequestHeader("Authorization")String token,String orderid){
        System.out.println("收货");
        int ihaveget = orderService.ihaveget(token, orderid);
        if (ihaveget>0){
            return "success";
        }
        else {
            return "fail";
        }
    }


    /**
     * 查看已完成的订单
     *
     */
    @GetMapping("/order/finished")
    public String finished(@RequestHeader("Authorization")String token){
        List<Orders> orders_finished = orderService.getorders_finished(token, "已完成");
        System.out.println(orders_finished);
        for (Orders o:orders_finished
        ) {
            String shoplogopath = o.getOrders().get(0).getShoplogopath();
            String substring = shoplogopath.substring(27);
            String newlogopath=Dictionary.ip+substring;
            o.getOrders().get(0).setShoplogopath(newlogopath);
        }
        System.out.println(orders_finished);
        Object o = JSON.toJSON(orders_finished);
        String orders2 = JSON.toJSONString(o);
        System.out.println(orders2);

        return orders2;
    }


}
