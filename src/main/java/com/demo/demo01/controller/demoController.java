package com.demo.demo01.controller;



import com.alibaba.fastjson.JSON;
import com.demo.demo01.pojo.Address;
import com.demo.demo01.pojo.AddressDTO1;
import com.demo.demo01.pojo.User;
import com.demo.demo01.service.UserService;
import com.demo.demo01.utils.Base64Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartRequest;

import javax.jws.WebParam;
import java.io.File;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/user")
public class demoController {

    @Autowired
    private UserService userService;

    /**
     * 注册用户
     * @param user
     * @return
     */

    @PostMapping("/reg")
    @ResponseBody
    public String reg(com.demo.demo01.pojo.User user){
        System.out.println(user.toString());
        int reguserresult = userService.Reguser(user);
        if (reguserresult==1){
            return "regsuccess";
        }
        else {
            return "regfailed";
        }

    }


    /**
     * 用户登陆
     * @param username
     * @param password
     * @return
     */
    @RequestMapping("/login")
    @ResponseBody
    public String login(String username,String password){
       User currentUser = userService.login(username);
       if (currentUser!=null){
           String passworddb = currentUser.getPassword();
           if (password.equals(passworddb)){
               System.out.println("登陆成功");
               String iconpath = currentUser.getIconpath();
               String base64str = Base64Util.convertFileToBase64(iconpath);
               currentUser.setIconpath(base64str);
               return JSON.toJSONString(currentUser);
           }
           else {
               System.out.println("密码错误");
               return "pwd_error";
           }
       }
       else {
           System.out.println("用户不存在");
           return "no_user";
       }


    }


    /**
     * 头像上传(返回给前端base64(更新storagesync)，并更新数据库中头像路径字段，待下次登陆时读取头像字段并转为base64)
     * @param myfile
     * @return
     * @throws IOException
     */
    @RequestMapping("/uploadhead")
    @ResponseBody
    //myfile要与前端的参数名一致
    public String uploadHead(MultipartFile myfile, @RequestHeader("Authorization")String token) throws IOException {
        if ("".equals(token)){
            return "no_token";
        }
        else {
            System.out.println(token);
            String name = myfile.getName();
            System.out.println(name);

            String path="D:\\webapp\\"+token+".jpg";
            File file = new File(path);
            myfile.transferTo(file);
            int i = userService.modifyHead(token, path);
            if (i==1){
                return Base64Util.convertFileToBase64(path);
            }
            else {
                return "update_fail";
            }

        }
    }


    /**
     * 修改昵称
     * @param new_nicheng
     * @param token
     * @return
     */
    @PostMapping("/modifynicheng")
    @ResponseBody
    public String modifynicheng(String new_nicheng,@RequestHeader("Authorization") String token ){
        System.out.println("昵称："+new_nicheng+"*" );
        System.out.println("进来了");
        if ("".equals(token)){
            return "no_token";
        }
        else {
            int modifynicheng = userService.modifynicheng(token, new_nicheng);
            if (modifynicheng==1){
                System.out.println("修改成功");
                return new_nicheng;
            }
            else {
                return "fail";
            }
        }

    }


    /**
     * 添加收货地址
     * @param address
     * @param token
     * @return
     */
    @PostMapping("/addAddress")
    @ResponseBody
    public String addAddress(Address address,@RequestHeader("Authorization") String token ){

        if ("".equals(token)){
            return "no_token";
        }
        else {
            address.setUuid(token);
            int i = userService.addAddress(address);
            if (i==1){
                return "success";
            }
            else {
                return "fail";
            }
        }

    }


    /**
     * 查看我的收获地址
     * @param token
     * @return
     */
    @GetMapping("/getHisAddress")
    @ResponseBody
    public String getHisAddress(@RequestHeader("Authorization") String token){
        if ("".equals(token)){
            return "no_token";
        }
        else {
            List<AddressDTO1> getaddresses = userService.Getaddresses(token);
            return JSON.toJSONString(getaddresses);
        }
    }


    /**
     * 修改收货地址
     * @param token
     * @param addressDTO1
     * @return
     */
    @PostMapping("/modifyAddress")
    @ResponseBody
    public String modifyAddress(@RequestHeader("Authorization") String token,AddressDTO1 addressDTO1){
        if ("".equals(token)){
            return "no_token";
        }
        else {
            int i = userService.modifyAddress(token, addressDTO1);
            if (i==1){
                return "success";
            }
            else {
                return "fail";
            }
        }


    }


    @PostMapping("/deleteAddress")
    @ResponseBody
    public String deleteAddress(@RequestHeader("Authorization") String token,String addressid){
        if ("".equals(token)){
            return "no_token";
        }
        else {
            int i = userService.deleteAddress(token, addressid);
            if (i==1){
                return "success";
            }
            else {
                return "fail";
            }
        }
    }


}
