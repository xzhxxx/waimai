package com.demo.demo01.controller;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradeAppPayModel;
import com.alipay.api.request.AlipayTradeAppPayRequest;
import com.alipay.api.response.AlipayTradeAppPayResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
public class PayController {

    @RequestMapping("/pay")
    public String getorderinfo(@RequestParam("shopname")String shopname,@RequestParam("totalprice") String totalprice){
//        System.out.println(shopname);
//        System.out.println(totalprice);

        //实例化客户端
        AlipayClient alipayClient = new DefaultAlipayClient("https://openapi.alipay.com/gateway.do", "2016103100780401", "MIIEwAIBADANBgkqhkiG9w0BAQEFAASCBKowggSmAgEAAoIBAQC85N0WPapl22uK//JntHP22qCvCikl3om8GrJiEtH7IYxUm7yW7sToxeqfQVera9EngHF6ykTQmBtza3ZWRzHTZRPfZgK0Btrz76A+8/kWyDfgOTfMd9fS6BuLOKVBvU55iNs1Z7aFLwz68maiugH9nAi5sAuRyS3M36GT4y2vAKVy/RicMJdvN2JOmDcN7893ydqfzaSwsYg37HTIOFj8z7VSCWfLd7k6SRb79s5kdDEhuNs8xEwcknaDWSxnAvbnXoccn1spF1B5ZM5nhsGxWrMx3UhN/88PLpzZ/OJGDDP32jW0NPSycgKeh0lyY/10oWDoIDWDhqY2K43ZPp0rAgMBAAECggEBAIQU/EtK/1WN4lslsVi2o8ffBDChSDiErYbsnANb6ZxSzw9yfqKDfwjNK8wEIsto7q1ScQdNXGUHl1pTsEGQMtUAfq7dY9KCYKPl0QV0D57QjdGp8Mx2mGGPj4MALv4S40a0XaOwS75BMjBfQ0lSC4ZMjwVYhh6AsBfr383LMuyEsT5fig3R6S80C+c8BMj8dgrxnifrm9WJTJUjLiSPihWPU6b60RRpkAKv8fq4+thgDNyJnYJm8X8fUkFvK97RxXbZ2hTZVDBbXt4CBQrJ3Ds1OmTYNFWwKanUDMaaxI3119iUKTHsYC+5WBp4zLLMLkTAnHOTiYrNIGYrUMqrUMECgYEA51f/LKb8K6yLrVBcCRJo8nJU0uaF6flM9sKTnFgmt8OSpjTTgO3U3LAVTaOei3P53tglUh3wWZvfhrYcsu/KP8Kt2gv5B8JuscJqWhu/s7pQpFjTVezHQ5VxMFIgpdaTEMNPKuSJjW0p9BkF0oXMIPHwslA1J0WuHkZ9mB8VPdsCgYEA0QaqFFQPqDtlYT8gZJyEtmOzY9hgCAZpNyLIOud5JwKvfoPuAVO3gZqXx0ZANYZJmgBtDn0iVeItGjs8EKHhgsu28E9A7YH9UxQ+ONSVCjR7IL798NPOJp6iP6KZX0dtkyobmIlRRcvy1/GkJjQmQF3qeL1vqHqQ0WO1pwm4xvECgYEA5aDP1WeOaF9laUuZfxu59eUMUyakB6EABvMOB6LsE31ZpAZNZe0sWz2gd6cP6QXeIJbVPJQhDSEOFy3OgMJJM06ut/8oWc5hh6odXIGsn94wy2CrrN9QzsGYT0lFxLae1YW3oDCmZC3V9cecouFRw+XCb4vzpp6gfMalRYic0f8CgYEAwPuxLYGKzxy/dfBNXPaxBJ+6MRXFW8NngtMnIFMzpNOy6VCu/iSnyQ3M0dvHTmOEMp3wEi9v8vEWCSDlTMnYpeSlKutRG7cnBnwrElcan0hi4ZHUbHNw4nEVhUD+71vIC9YVklg9Y/5vX/YmQmtlYYoT/5OOsnMun1O6k9TC91ECgYEAlh4HC2AEDK6xlXQyT0dZgHzh+qIs3Hg9rIgGmpsOefBHW1YHQjkduh0SHl5utLkg2nimVhYvAl2b0e0gpXOexQzn5BnO9qIBlTxqmbJssNMzrbgVx8gT020DpTDdqZjCylJZNtwke9/nBVrtlF4gF7/zDj1Rpvrjs1wzzyLxLck=", "json", "utf-8", "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvOTdFj2qZdtriv/yZ7Rz9tqgrwopJd6JvBqyYhLR+yGMVJu8lu7E6MXqn0FXq2vRJ4BxespE0Jgbc2t2Vkcx02UT32YCtAba8++gPvP5Fsg34Dk3zHfX0ugbizilQb1OeYjbNWe2hS8M+vJmoroB/ZwIubALkcktzN+hk+MtrwClcv0YnDCXbzdiTpg3De/Pd8nan82ksLGIN+x0yDhY/M+1Uglny3e5OkkW+/bOZHQxIbjbPMRMHJJ2g1ksZwL2516HHJ9bKRdQeWTOZ4bBsVqzMd1ITf/PDy6c2fziRgwz99o1tDT0snICnodJcmP9dKFg6CA1g4amNiuN2T6dKwIDAQAB", "RSA2");
//实例化具体API对应的request类,类名称和接口名称对应,当前调用接口名称：alipay.trade.app.pay
        AlipayTradeAppPayRequest request = new AlipayTradeAppPayRequest();
//SDK已经封装掉了公共参数，这里只需要传入业务参数。以下方法为sdk的model入参方式(model和biz_content同时存在的情况下取biz_content)。
        AlipayTradeAppPayModel model = new AlipayTradeAppPayModel();
        model.setBody("我是测试数据");
        model.setSubject(shopname);
        model.setOutTradeNo((Long.valueOf(String.valueOf((new Date()).getTime()))).toString());
        model.setTimeoutExpress("30m");
        model.setTotalAmount(totalprice);
        model.setProductCode("QUICK_MSECURITY_PAY");
        request.setBizModel(model);
        request.setNotifyUrl("商户外网可以访问的异步地址");
        try {
            //这里和普通的接口调用不同，使用的是sdkExecute
            AlipayTradeAppPayResponse response = alipayClient.sdkExecute(request);
            System.out.println(response.getBody());//就是orderString 可以直接给客户端请求，无需再做处理。
            return response.getBody();


        } catch (AlipayApiException e) {
            e.printStackTrace();
            return "none";
        }


    }

}
