package com.demo.demo01.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.demo.demo01.pojo.Product;
import com.demo.demo01.pojo.ProductDTO1;
import com.demo.demo01.pojo.Shop;
import com.demo.demo01.service.ShopService;
import com.demo.demo01.utils.Base64Util;
import com.demo.demo01.utils.Dictionary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/shop")
public class ShopController {

    @Autowired
    ShopService shopService;

    @GetMapping("/getshops")
    public String getshops(){
        System.out.println("商店");
        List<Shop> shops = shopService.getShops();
        for (Shop s:shops
             ) {
            String shoplogopath = s.getShoplogopath();
            String s1 = Base64Util.convertFileToBase64(shoplogopath);
            s1 = s1.replaceAll("\r\n", "");
            String s2="data:image/png;base64,"+s1;
            s.setShoplogopath(s2);
        }
        return JSON.toJSONString(shops);
    }


    @GetMapping("/shopdetail")
    public String shopdetail(String shopid){
        List<Product> products = shopService.getShopDetail(shopid);
        System.out.println(products.toString());
        for (Product p:products
             ) {
            String thingpic = p.getThingpic();
//            String s1 = Base64Util.convertFileToBase64(thingpic);
//            s1 = s1.replaceAll("\r\n", "");
//            String s2="data:image/png;base64,"+s1;
            System.out.println(thingpic);
            String substring = thingpic.substring(27);
            System.out.println(substring);
            p.setThingpic(Dictionary.ip+substring);
        }

        JSONArray jsonArray = new JSONArray();
        String productsStr=JSON.toJSONString(products);//本店产品数据字符串 json

        String headpath = shopService.shopHeadPic(shopid);
//        String tmp = Base64Util.convertFileToBase64(headpath);
//        String headbase64 = tmp.replaceAll("\r\n", "");
//        String s2="data:image/png;base64,"+headbase64;
        String substring = headpath.substring(27);
        String s2=Dictionary.ip+substring;
        jsonArray.add(0,s2);
        jsonArray.add(1,productsStr);

        System.out.println(JSON.toJSONString(jsonArray));
        return JSON.toJSONString(jsonArray);

    }



    //将物品信息添加到该用户对应的该商店的购物车中
    @PostMapping("/AddToThisShop_Car")
    public String AddToThisShop_Car(String data){
        System.out.println(data);
        return "添加物品到购物车";
    }


}
