package com.demo.demo01.dao;

import com.demo.demo01.pojo.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OrderDao {
    int createOrder(@Param("orderid") String orderid,@Param("submiterid") String submiterid,@Param("shopid")String shopid,@Param("totalcount")String totalcount,  @Param("sendaddress") SendAddress sendAddress);
    int addOrder_detail(@Param("orderid") String orderid, @Param("productdto") List<ProductDTO1> productDTO1);

    //查找当前用户的全部订单
    List<Orders> getorders(String token);

    OrderDetail getcurrentOrder(@Param("token") String token,@Param("orderid")String orderid);

    //修改订单状态(支付完成)
    int UpdateOrder_payed(String orderid);

    //查找当前用户的待消费订单(待支付或已完成)
    List<Orders> getorders_finished(@Param("token") String token,@Param("state")String state);


    //查看已下单和已送出的订单
    List<Orders> getorders_xiadan_send(@Param("token") String token,@Param("state")String state,@Param("state2")String state2);

    //收货操作
    int ihaveget(@Param("token")String token,@Param("orderid")String orderid);




}
