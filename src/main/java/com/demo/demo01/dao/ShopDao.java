package com.demo.demo01.dao;

import com.demo.demo01.pojo.Product;
import com.demo.demo01.pojo.Shop;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ShopDao {
    List<Shop> getShops();

    List<Product> getShopDetail(String shopuuid);

    String shopHeadPic(String shopid);

}
