package com.demo.demo01.dao;

import com.demo.demo01.pojo.Address;
import com.demo.demo01.pojo.AddressDTO1;
import com.demo.demo01.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.annotation.MapperScan;

import java.util.List;

@Mapper
public interface UserDao {
    int regUser(User user);
    User login(String username);
    int modifyHead(@Param("uuid") String uuid, @Param("iconpath") String iconpath);
    int modifynicheng(@Param("uuid") String uuid, @Param("newnicheng") String newincheng);

    int addAddress(Address address);

    List<AddressDTO1> Getaddresses(String token);
    int modifyAddress(@Param("uuid") String uuid,@Param("dto1")AddressDTO1 dto1);
    int deleteAddress(@Param("uuid") String uuid,@Param("addressid") String addressid);

    //查找有没有这个人
    User getuser(String uuid);

}
