package com.demo.demo01.serviceImpl;

import com.demo.demo01.dao.UserDao;
import com.demo.demo01.pojo.Address;
import com.demo.demo01.pojo.AddressDTO1;
import com.demo.demo01.pojo.User;
import com.demo.demo01.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;

    @Override
    public int Reguser(com.demo.demo01.pojo.User user) {
        return userDao.regUser(user);
    }

    @Override
    public User login(String username) {
        return userDao.login(username);
    }

    @Override
    public int modifyHead(String uuid, String iconpath) {
        return userDao.modifyHead(uuid, iconpath);
    }

    @Override
    public int modifynicheng(String uuid, String newnicheng) {
        return userDao.modifynicheng(uuid, newnicheng);
    }

    @Override
    public int addAddress(Address address) {
        return userDao.addAddress(address);
    }

    @Override
    public List<AddressDTO1> Getaddresses(String token) {
        return userDao.Getaddresses(token);
    }

    @Override
    public int modifyAddress(String uuid, AddressDTO1 dto1) {
        return userDao.modifyAddress(uuid, dto1);
    }

    @Override
    public int deleteAddress(String uuid, String addressid) {
        return userDao.deleteAddress(uuid, addressid);
    }

    @Override
    public User getuser(String uuid) {
        return userDao.getuser(uuid);
    }


}
