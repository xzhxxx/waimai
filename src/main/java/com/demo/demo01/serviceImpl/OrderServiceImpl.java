package com.demo.demo01.serviceImpl;

import com.demo.demo01.dao.OrderDao;
import com.demo.demo01.pojo.*;
import com.demo.demo01.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {

    @Autowired
    OrderDao orderDao;

    @Override
    public int createOrder(String orderid,String submiterid,String shopid, String totalcount,SendAddress sendAddress, List<ProductDTO1> productDTO1) {
        orderDao.createOrder(orderid,submiterid,shopid,totalcount,sendAddress);
        int i=orderDao.addOrder_detail(orderid, productDTO1);
        return i;
    }

    @Override
    public List<Orders> getorders(String token) {
        return orderDao.getorders(token);
    }

    @Override
    public OrderDetail getcurrentOrder(String token, String orderid) {
        return orderDao.getcurrentOrder(token, orderid);
    }

    @Override
    public int UpdateOrder_payed(String orderid) {
        return orderDao.UpdateOrder_payed(orderid);
    }

    @Override
    public List<Orders> getorders_finished(String token,String state) {
        return orderDao.getorders_finished(token,state);
    }

    @Override
    public List<Orders> getorders_xiadan_send(String token, String state, String state2) {
        return orderDao.getorders_xiadan_send(token, state, state2);
    }

    @Override
    public int ihaveget(String token, String orderid) {
        return orderDao.ihaveget(token, orderid);
    }


}
