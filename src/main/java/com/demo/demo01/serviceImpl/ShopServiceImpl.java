package com.demo.demo01.serviceImpl;

import com.demo.demo01.dao.ShopDao;
import com.demo.demo01.pojo.Product;
import com.demo.demo01.pojo.Shop;
import com.demo.demo01.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShopServiceImpl implements ShopService {

    @Autowired
    ShopDao shopDao;

    @Override
    public List<Shop> getShops() {
        return shopDao.getShops();
    }

    @Override
    public List<Product> getShopDetail(String shopuuid) {
        return shopDao.getShopDetail(shopuuid);
    }

    @Override
    public String shopHeadPic(String shopid) {
        return shopDao.shopHeadPic(shopid);
    }
}
